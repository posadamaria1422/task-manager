package com.posadamaria.misiontic.taskmanager.model;

import com.posadamaria.misiontic.taskmanager.mvp.MainMVP;
import com.posadamaria.misiontic.taskmanager.view.dto.TaskItem;

import java.util.ArrayList;
import java.util.List;

public class MainInteractor implements MainMVP.Model {

    private List<TaskItem> temItems;

    public MainInteractor() {
        temItems = new ArrayList<>();
        temItems.add(new TaskItem("Do the shopping", "November 20, 2021"));
    }

    @Override
    public List<TaskItem> getTask() {
        return new ArrayList<>(temItems);
    }

    @Override
    public void saveTask(TaskItem task) {
        temItems.add(task);
    }

    @Override
    public void updateTask(TaskItem item) {

    }

    @Override
    public void deleteTask(TaskItem task) {

    }
}
