package com.posadamaria.misiontic.taskmanager.presenter;

import com.posadamaria.misiontic.taskmanager.model.RegisterInteractor;
import com.posadamaria.misiontic.taskmanager.mvp.RegisterMVP;

public class RegisterPresenter implements RegisterMVP.Presenter {

    private final RegisterMVP.View view;
    private final RegisterMVP.Model model;

    public RegisterPresenter(RegisterMVP.View view) {
        this.view = view;
        this.model = new RegisterInteractor(view.getActivity());
    }

    @Override
    public void onRegisterClick() {
        boolean error = false;
        RegisterMVP.RegisterInfo registerInfo = view.getRegisterInfo();

        //Validación de datos
        view.showEmailError("");
        view.showPasswordError("");

        if (registerInfo.getEmail().isEmpty()) {
            view.showEmailError("Correo electrónico es requerido");
            error = true;
        } else if (!isEmailValid(registerInfo.getEmail())) {
            view.showEmailError("Correo electrónico no es válido");
            error = true;
        }

        if (registerInfo.getPassword().isEmpty()) {
            view.showPasswordError("Contraseña es requerida");
            error = true;
        } else if (!isPasswordValid(registerInfo.getPassword())) {
            view.showPasswordError("Contraseña no cumple criterios de seguridad debe tener caracteres alfanumericos");
            error = true;
        }

        if (!error) {
            view.showProgressBar();
            new Thread(() -> model.validateCredentials(registerInfo.getEmail(), registerInfo.getPassword(),
                    new RegisterMVP.Model.ValidateCredentialsCallback() {
                        @Override
                        public void onSuccess() {
                            view.getActivity().runOnUiThread(() -> {
                                view.hideProgressBar();
                                view.showMainActivity();
                            });
                        }

                        @Override
                        public void onFailure() {
                            view.getActivity().runOnUiThread(() -> {
                                view.hideProgressBar();
                                view.showGeneralError("Credenciales inválidas");
                            });
                        }
                    })).start();
        }
    }

    @Override
    public void validateHasUsersAuthenticated() {
        if (model.hasAuthenticatedUser()) {
            view.showMainActivity();

        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 6;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@")
                && email.endsWith(".com") || email.endsWith(".co");
    }

}

