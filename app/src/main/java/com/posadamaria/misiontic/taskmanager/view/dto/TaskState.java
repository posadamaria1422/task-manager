package com.posadamaria.misiontic.taskmanager.view.dto;

public enum TaskState {
    PENDING,
    DONE
}
