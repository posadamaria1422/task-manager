package com.posadamaria.misiontic.taskmanager.model.repository;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseAuthRepository {

    private static FirebaseAuthRepository instance;

    public static FirebaseAuthRepository getInstance() {
        if (instance == null) {
            instance = new FirebaseAuthRepository();
        }
        return instance;
    }

    private final FirebaseAuth mAuth;
    private FirebaseUser currentUser;

    private FirebaseAuthRepository() {
        mAuth = FirebaseAuth.getInstance();

    }

    public boolean isAuthenticated() {
        return getCurrentUser() != null;
    }

    public FirebaseUser getCurrentUser() {
        if (currentUser == null) {
            currentUser = mAuth.getCurrentUser();
        }
        return currentUser;
    }

    public void logOut() {
        if (currentUser != null) {
            mAuth.signOut();
            currentUser = null;
        }
    }

    public void createUser(String email, String password, FirebaseAuthCallback callback) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser = mAuth.getCurrentUser();
                        callback.onSuccess();
                    } else {
                        callback.onFailure();
                    }
                });
    }

    public void validateUser(String email, String password, FirebaseAuthCallback callback) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser = mAuth.getCurrentUser();
                        callback.onSuccess();
                    } else {
                        callback.onFailure();
                    }
                });
    }

    public interface FirebaseAuthCallback {
        void onSuccess();

        void onFailure();
    }
}
