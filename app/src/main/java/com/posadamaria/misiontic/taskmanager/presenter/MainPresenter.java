package com.posadamaria.misiontic.taskmanager.presenter;

import android.util.Log;

import com.posadamaria.misiontic.taskmanager.R;
import com.posadamaria.misiontic.taskmanager.model.MainInteractor;
import com.posadamaria.misiontic.taskmanager.mvp.MainMVP;
import com.posadamaria.misiontic.taskmanager.view.dto.TaskItem;
import com.posadamaria.misiontic.taskmanager.view.dto.TaskState;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MainPresenter implements MainMVP.Presenter {

    private final MainMVP.View view;
    private final MainMVP.Model model;

    public MainPresenter(MainMVP.View view) {
        this.view = view;
        this.model = new MainInteractor();
    }

    @Override
    public void loadTask() {
        List<TaskItem> items = model.getTask();

        view.showTaskList(items);
    }

    @Override
    public void addNewTask() {
        Log.i(MainPresenter.class.getSimpleName(), "Add new Task");
        String description = view.getTaskDescription();
        String date = SimpleDateFormat.getDateInstance().format(new Date());

        TaskItem task = new TaskItem(description, date);
        model.saveTask(task);
        view.addTalkToList(task);
    }

    @Override
    public void taskItemClicked(TaskItem task) {
        String message = task.getState() == TaskState.PENDING
                ? "Desea marcar como terminada esta tarea"
                : "Desea marcar como pendiente esta tarea";
        view.showConfirmDialog(message, task);
    }

    @Override
    public void updateTask(TaskItem task) {
        task.setState(task.getState() == TaskState.PENDING ? TaskState.DONE : TaskState.PENDING);

        model.updateTask(task);
        view.updateTask(task);
    }

    @Override
    public void taskItemLongClicked(TaskItem task) {
        if (task.getState() == TaskState.DONE) {
            view.showDeleteDialog("Desea eliminar esta tarea", task);
        }
    }

    @Override
    public void deleteTask(TaskItem task) {
        model.deleteTask(task);
        view.deleteTask(task);
    }
}
