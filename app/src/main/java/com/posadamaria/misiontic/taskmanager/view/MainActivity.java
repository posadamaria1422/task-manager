package com.posadamaria.misiontic.taskmanager.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.posadamaria.misiontic.taskmanager.R;
import com.posadamaria.misiontic.taskmanager.model.repository.FirebaseAuthRepository;
import com.posadamaria.misiontic.taskmanager.mvp.MainMVP;
import com.posadamaria.misiontic.taskmanager.presenter.MainPresenter;
import com.posadamaria.misiontic.taskmanager.view.adapter.TaskAdapter;
import com.posadamaria.misiontic.taskmanager.view.dto.TaskItem;

import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements MainMVP.View {

    private TextInputLayout tilNewTask;
    private TextInputEditText etNewTask;
    private RecyclerView rvTasks;

    private TaskAdapter taskAdapter;

    private MainMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenter(MainActivity.this);

        initUI();
        presenter.loadTask();
    }
// esto va en un MVP
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.app_name)
                .setMessage("¿Esta seguro de cerrar sesión?")
                .setPositiveButton("Si",
                        (dialog, which) -> {
                            FirebaseAuthRepository repository = FirebaseAuthRepository.getInstance();
                            repository.logOut();

                            MainActivity.super.onBackPressed();
                        })
                .setNegativeButton("No", null);
        builder.create().show();
    }

    private void initUI() {
        tilNewTask = findViewById(R.id.til_new_task);
        tilNewTask.setEndIconOnClickListener(v -> presenter.addNewTask());

        etNewTask = findViewById(R.id.et_new_task);

        taskAdapter = new TaskAdapter();
        taskAdapter.setClickListener(item -> presenter.taskItemClicked(item));
        taskAdapter.setLongClickListener(item -> presenter.taskItemLongClicked(item));

        rvTasks = findViewById(R.id.rv_tasks);
        rvTasks.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        rvTasks.setAdapter(taskAdapter);
    }

    @Override
    public void showTaskList(List<TaskItem> items) {
        taskAdapter.setData(items);
    }

    @Override
    public String getTaskDescription() {
        return Objects.requireNonNull(etNewTask.getText()).toString();
    }

    @Override
    public void addTalkToList(TaskItem task) {
        taskAdapter.addItem(task);
    }

    @Override
    public void updateTask(TaskItem task) {
        taskAdapter.updateTask(task);
    }

    @Override
    public void showConfirmDialog(String message, TaskItem task) {
        new AlertDialog.Builder(this)
                .setIcon(R.mipmap.ic_warning)
                .setTitle("Task selected")
                .setMessage(message)
                .setPositiveButton("Yes", ((dialog, which) -> presenter.updateTask(task)))
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public void showDeleteDialog(String message, TaskItem task) {
        new AlertDialog.Builder(this)
                .setIcon(R.mipmap.ic_warning)
                .setTitle("Task selected")
                .setMessage(message)
                .setPositiveButton("Yes", ((dialog, which) -> presenter.deleteTask(task)))
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public void deleteTask(TaskItem task) {
        taskAdapter.removeTask(task);
    }
}