package com.posadamaria.misiontic.taskmanager.mvp;

import android.app.Activity;

public interface RegisterMVP {
    interface Model {

        void validateCredentials(String email, String password,
                                 ValidateCredentialsCallback callback);


        boolean hasAuthenticatedUser();

        interface ValidateCredentialsCallback {
            void onSuccess();

            void onFailure();
        }

    }

    interface Presenter {

        void onRegisterClick();

        void validateHasUsersAuthenticated();
    }

    interface View {
        Activity getActivity();

        RegisterInfo getRegisterInfo();

        void showPasswordError(String error);

        void showEmailError(String error);

        void showMainActivity();

        void showGeneralError(String error);

        void showProgressBar();

        void hideProgressBar();

    }

    class RegisterInfo {
        private final String email;
        private final String password;

        public RegisterInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}
