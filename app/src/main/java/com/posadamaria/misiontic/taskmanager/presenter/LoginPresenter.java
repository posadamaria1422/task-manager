package com.posadamaria.misiontic.taskmanager.presenter;

import com.posadamaria.misiontic.taskmanager.model.LoginInteractor;
import com.posadamaria.misiontic.taskmanager.mvp.LoginMVP;


public class LoginPresenter implements LoginMVP.Presenter {

    private final LoginMVP.View view;
    private final LoginMVP.Model model;

    public LoginPresenter(LoginMVP.View view) {
        this.view = view;
        this.model = new LoginInteractor(view.getActivity());
    }

    @Override
    public void onLoginClick() {
        boolean error = false;
        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();

        //Validación de datos
        view.showEmailError("");
        view.showPasswordError("");

        if (loginInfo.getEmail().isEmpty()) {
            view.showEmailError("Correo electrónico es requerido");
            error = true;
        } else if (!isEmailValid(loginInfo.getEmail())) {
            view.showEmailError("Correo electrónico no es válido");
            error = true;
        }

        if (loginInfo.getPassword().isEmpty()) {
            view.showPasswordError("Contraseña es requerida");
            error = true;
        } else if (!isPasswordValid(loginInfo.getPassword())) {
            view.showPasswordError("Contraseña no cumple criterios de seguridad debe tener caracteres alfanumericos");
            error = true;
        }

        if (!error) {
            view.showProgressBar();
            new Thread(() -> model.validateCredentials(loginInfo.getEmail(), loginInfo.getPassword(),
                    new LoginMVP.Model.ValidateCredentialsCallback() {
                        @Override
                        public void onSuccess() {
                            view.getActivity().runOnUiThread(() -> {
                                view.hideProgressBar();
                                view.showMainActivity();
                            });
                        }

                        @Override
                        public void onFailure() {
                            view.getActivity().runOnUiThread(() -> {
                                view.hideProgressBar();
                                view.showGeneralError("Credenciales inválidas");
                            });
                        }
                    })).start();
        }
    }

    @Override
    public void onRegisterClick() {
        view.showRegisterActivity();
    }

    @Override
    public void validateHasUsersAuthenticated() {
        if (model.hasAuthenticatedUser()) {
            view.showMainActivity();
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 6;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@")
                && email.endsWith(".com") || email.endsWith(".co");
    }

}
