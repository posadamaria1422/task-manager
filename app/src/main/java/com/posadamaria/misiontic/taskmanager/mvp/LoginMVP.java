package com.posadamaria.misiontic.taskmanager.mvp;

import android.app.Activity;

public interface LoginMVP {
    interface Model {

        void validateCredentials(String email, String password,
                                 ValidateCredentialsCallback callback);


        boolean hasAuthenticatedUser();

        interface ValidateCredentialsCallback {
            void onSuccess();

            void onFailure();
        }

    }

    interface Presenter {
        void onLoginClick();

        void onRegisterClick();

        void validateHasUsersAuthenticated();
    }

    interface View {
        Activity getActivity();

        LoginInfo getLoginInfo();

        void showPasswordError(String error);

        void showEmailError(String error);

        void showMainActivity();

        void showRegisterActivity();

        void showGeneralError(String error);

        void showProgressBar();

        void hideProgressBar();

    }

    class LoginInfo {
        private final String email;
        private final String password;

        public LoginInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }

}
