package com.posadamaria.misiontic.taskmanager.model;

import android.content.Context;
import android.util.Log;

import com.posadamaria.misiontic.taskmanager.model.repository.FirebaseAuthRepository;
import com.posadamaria.misiontic.taskmanager.mvp.LoginMVP;

public class LoginInteractor implements LoginMVP.Model {

    private final FirebaseAuthRepository firebaseAuthRepository;

    public LoginInteractor(Context context) {
        firebaseAuthRepository = FirebaseAuthRepository.getInstance();
    }

    @Override
    public void validateCredentials(String email, String password,
                                    ValidateCredentialsCallback callback) {

        // Validación de usuario
        firebaseAuthRepository.validateUser(email, password,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        Log.i(LoginInteractor.class.getSimpleName(), "Usuario existente");
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        Log.e(LoginInteractor.class.getSimpleName(), "El usuario no existe");
                        callback.onFailure();
                    }
                });
    }

    @Override
    public boolean hasAuthenticatedUser() {
        return firebaseAuthRepository.isAuthenticated();
    }
}
