package com.posadamaria.misiontic.taskmanager.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.posadamaria.misiontic.taskmanager.R;
import com.posadamaria.misiontic.taskmanager.mvp.RegisterMVP;
import com.posadamaria.misiontic.taskmanager.presenter.RegisterPresenter;

import java.util.Objects;

public class RegisterActivity extends AppCompatActivity implements RegisterMVP.View {


    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;
    private TextInputLayout tilPassword;
    private TextInputEditText etPassword;

    private LinearProgressIndicator pbWait;

    private AppCompatButton btnRegister;

    private RegisterMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        presenter = new RegisterPresenter(this);
        presenter.validateHasUsersAuthenticated();

        initUI();
    }

    private void initUI() {
        tilEmail = findViewById(R.id.til_email);
        etEmail = findViewById(R.id.et_email);

        tilPassword = findViewById(R.id.til_password);
        etPassword = findViewById(R.id.et_password);

        pbWait = findViewById(R.id.pb_wait);

        btnRegister = findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(v -> presenter.onRegisterClick());
    }

    @Override
    public Activity getActivity() {
        return RegisterActivity.this;
    }

    @Override
    public RegisterMVP.RegisterInfo getRegisterInfo() {
        return new RegisterMVP.RegisterInfo(
                Objects.requireNonNull(etEmail.getText()).toString().trim(),
                Objects.requireNonNull(etPassword.getText()).toString().trim());
    }

    @Override
    public void showPasswordError(String error) {
        tilPassword.setError(error);
    }

    @Override
    public void showEmailError(String error) {
        tilEmail.setError(error);
    }

    @Override
    public void showMainActivity() {
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);

    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(RegisterActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressBar() {
        pbWait.setVisibility(View.VISIBLE);
        btnRegister.setEnabled(false);
    }

    @Override
    public void hideProgressBar() {
        pbWait.setVisibility(View.GONE);
        btnRegister.setEnabled(true);
    }
}