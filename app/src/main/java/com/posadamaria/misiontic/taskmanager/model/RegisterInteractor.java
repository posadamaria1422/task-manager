package com.posadamaria.misiontic.taskmanager.model;

import android.content.Context;
import android.util.Log;

import com.posadamaria.misiontic.taskmanager.model.repository.FirebaseAuthRepository;
import com.posadamaria.misiontic.taskmanager.mvp.RegisterMVP;

public class RegisterInteractor implements RegisterMVP.Model {

    private final FirebaseAuthRepository firebaseAuthRepository;

    public RegisterInteractor(Context context) {
        firebaseAuthRepository = FirebaseAuthRepository.getInstance();
    }

    @Override
    public void validateCredentials(String email, String password,
                                    ValidateCredentialsCallback callback) {

        // Creación de usuario
        firebaseAuthRepository.createUser(email, password,
                new FirebaseAuthRepository.FirebaseAuthCallback() {
                    @Override
                    public void onSuccess() {
                        Log.i(RegisterInteractor.class.getSimpleName(), "Usuario creado exitosamente");
                        callback.onSuccess();
                    }

                    @Override
                    public void onFailure() {
                        Log.e(RegisterInteractor.class.getSimpleName(), "El usuario no pudo  ser creado");
                        callback.onFailure();
                    }
                });
    }

    @Override
    public boolean hasAuthenticatedUser() {
        return firebaseAuthRepository.isAuthenticated();
    }
}

